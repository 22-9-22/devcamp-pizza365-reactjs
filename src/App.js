
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import HeaderComponent from "./components/header/headerComponent";
import FooterComponent from "./components/footer/footerComponent";
import Content from "./components/content/content/content";

function App() {
  return (
    <div className="container">
      <HeaderComponent/>
      <Content/>
      <FooterComponent/>
    </div>
  );
}

export default App;
