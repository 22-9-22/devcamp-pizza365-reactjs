import { Component } from "react";

class Drink extends Component {
  render() {
    return (
      <div>
        <div className="row">
          <div className="col-sm-12 text-center p-4 mt-4 text-warning">
            <h2>
              <b className="p-2 border-bottom">Chọn loại đồ uống</b>
            </h2>
          </div>
          <div className="col-sm-12">
            <select name="drink" id="select-drink" className="form-control">
              <option value="">Vui lòng chọn loại đồ uống</option>
            </select>
          </div>
        </div>
      </div>
    );
  }
}

export default Drink