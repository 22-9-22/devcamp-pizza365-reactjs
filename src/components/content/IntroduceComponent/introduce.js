import { Component } from "react";
import Image1 from "../../../assets/images/1.jpg";
import Image2 from "../../../assets/images/2.jpg";
import Image3 from "../../../assets/images/3.jpg";
import Image4 from "../../../assets/images/4.jpg";
import Image5 from "../../../assets/images/5.jpg";

class Introduce extends Component {
  render() {
    return (
      <div>
        <div className="row">
          <div className="col-sm-12 text-warning">
            <h1>
              <b>Pizza 365</b>
            </h1>
            <h5>
              <i>Truly italian !</i>
            </h5>
          </div>
          <div className="col-sm-12">
            <div
              id="carouselExampleIndicators"
              className="carousel slide"
              data-ride="carousel"
            >
              <ol className="carousel-indicators">
                <li
                  data-target="#carouselExampleIndicators"
                  data-slide-to="0"
                  className="active"
                ></li>
                <li
                  data-target="#carouselExampleIndicators"
                  data-slide-to="1"
                ></li>
                <li
                  data-target="#carouselExampleIndicators"
                  data-slide-to="2"
                ></li>
                <li
                  data-target="#carouselExampleIndicators"
                  data-slide-to="3"
                ></li>
                <li
                  data-target="#carouselExampleIndicators"
                  data-slide-to="4"
                ></li>
              </ol>
              <div className="carousel-inner">
                <div className="carousel-item active">
                  <img
                    className="d-block w-100"
                    src={Image1}
                    alt="First slide"
                  ></img>
                </div>
                <div className="carousel-item">
                  <img
                    className="d-block w-100"
                    src={Image2}
                    alt="Second slide"
                  ></img>
                </div>
                <div className="carousel-item">
                  <img
                    className="d-block w-100"
                    src={Image3}
                    alt="Third slide"
                  ></img>
                </div>
                <div className="carousel-item">
                  <img
                    className="d-block w-100"
                    src={Image4}
                    alt="Fourth slide"
                  ></img>
                </div>
                <div className="carousel-item">
                  <img
                    className="d-block w-100"
                    src={Image5}
                    alt="Fivth slide"
                  ></img>
                </div>
              </div>
              <a
                className="carousel-control-prev"
                href="#carouselExampleIndicators"
                role="button"
                data-slide="prev"
              >
                <span
                  className="carousel-control-prev-icon"
                  aria-hidden="true"
                ></span>
                <span className="sr-only">Previous</span>
              </a>
              <a
                className="carousel-control-next"
                href="#carouselExampleIndicators"
                role="button"
                data-slide="next"
              >
                <span
                  className="carousel-control-next-icon"
                  aria-hidden="true"
                ></span>
                <span className="sr-only">Next</span>
              </a>
            </div>
          </div>
          <div className="col-sm-12 p-4 mt-4 text-center text-warning">
            <h2>
              <b className="p-2 border-bottom">Tại sao lại Pizza 365</b>
            </h2>
          </div>
          <div className="col-sm-12">
            <div className="row">
              <div className="col-sm-3 p-4 why-1st border border-warning">
                <h3 className="p-2">Đa dạng</h3>
                <p className="p-2">
                  Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot nhất
                  hiện nay
                </p>
              </div>
              <div className="col-sm-3 p-4 why-2st border border-warning">
                <h3 className="p-2">Chất lượng</h3>
                <p className="p-2">
                  Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo
                  vệ sinh an toàn thực phẩm.
                </p>
              </div>
              <div className="col-sm-3 p-4 why-3st border border-warning">
                <h3 className="p-2">Hương vị</h3>
                <p className="p-2">
                  Đảm bảo hương vị ngon, độc, lạ mà bạn chỉ có thể trải nghiệm
                  từ Pizza 365.
                </p>
              </div>
              <div className="col-sm-3 p-4 why-4st border border-warning">
                <h3 className="p-2">Dịch vụ</h3>
                <p className="p-2">
                  Nhân viên thân thiện, nhà hàng hiện đại.Dịch vụ giao hàngnhanh
                  chất lương, tân tiến.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Introduce