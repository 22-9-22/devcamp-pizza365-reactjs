import { Component } from "react";

class Type extends Component {
  render() {
    return (
      <div>
        <div id="type" className="row">
          <div className="col-sm-12 text-center p-4 mt-4 text-warning">
            <h2>
              <b className="p-2 border-bottom">Chọn loại pizza</b>
            </h2>
          </div>
          <div className="col-sm-12">
            <div className="row">
              <div className="col-sm-4">
                <div className="card w-100" style={{ width: "18rem" }}>
                  <img src={ImageSeafood} className="card-img-top"></img>
                  <div className="card-body">
                    <h5>OCEAN MANIA</h5>
                    <p>PIZZA HAỈ SẢN SỐT MAYONNAISE</p>
                    <p>
                      Xốt cà chua, phô mai Mozzarella, Tôm, Mực, Thanh cua, Hành
                      tây.
                    </p>
                    <p>
                      <button
                        className="btn btn-pizza-type w-100"
                        id="btn-mania-type"
                      >
                        Chọn
                      </button>
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-sm-4">
                <div className="card w-100" style={{ width: "18rem" }}>
                  <img src={ImageHawaii} className="card-img-top"></img>
                  <div className="card-body">
                    <h5>HAWAIIAN</h5>
                    <p>PIZZA DĂM BÔNG DƯA KIỂU HAWAII</p>
                    <p>Xốt cà chua, Phô Mai Mozzarella, Thịt Dăm Bông, Thơm.</p>
                    <p>
                      <button
                        className="btn btn-pizza-type w-100"
                        id="btn-hawaii-type"
                      >
                        Chọn
                      </button>
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-sm-4">
                <div className="card w-100" style={{ width: "18rem" }}>
                  <img src={ImageBacon} className="card-img-top"></img>
                  <div className="card-body">
                    <h5>CHEESY CHICKEN BACON</h5>
                    <p>PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI</p>
                    <p>
                      Xốt Phô mai, thịt gà, Thịt heo muối, Phô mai Mozzarella,
                      Cà chua.
                    </p>
                    <p>
                      <button
                        className="btn btn-pizza-type w-100"
                        id="btn-bacon-type"
                      >
                        Chọn
                      </button>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Type