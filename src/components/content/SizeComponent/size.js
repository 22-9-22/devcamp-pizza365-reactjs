import { Component } from "react";

class Size extends Component {
  render() {
    return (
      <div>
        <div id="size" className="row">
          <div className="col-sm-12 text-center p-4 mt-4 text-warning">
            <h2>
              <b className="p-1 border-bottom">Chọn size pizza</b>
            </h2>
            <p>
              <span className="p-2">Hãy chọn cỡ pizza phù hợp với bạn!.</span>
            </p>
          </div>
          <div className="col-sm-12">
            <div className="row">
              <div className="col-sm-4">
                <div className="card">
                  <div className="card-header card-header-small text-center">
                    <h3>S (small)</h3>
                  </div>
                  <div className="card-body text-center">
                    <ul className="list-group list-group-flush">
                      <li className="list-group-item">
                        Đường kính: <b>20cm</b>
                      </li>
                      <li className="list-group-item">
                        Sườn nướng: <b>2</b>
                      </li>
                      <li className="list-group-item">
                        Salad: <b>200g</b>
                      </li>
                      <li className="list-group-item">
                        Nước ngọt: <b>2</b>
                      </li>
                      <li className="list-group-item">
                        <h1>
                          <b>150.000</b>
                        </h1>
                        <p>VNĐ</p>
                      </li>
                    </ul>
                  </div>
                  <div className="card-footer text-center">
                    <button
                      className="btn btn-pizza btn-block"
                      id="btn-pizza-small"
                    >
                      Chọn
                    </button>
                  </div>
                </div>
              </div>
              <div className="col-sm-4">
                <div className="card">
                  <div className="card-header card-header-medium text-center">
                    <h3>M (medium)</h3>
                  </div>
                  <div className="card-body text-center">
                    <ul className="list-group list-group-flush">
                      <li className="list-group-item">
                        Đường kính: <b>25cm</b>
                      </li>
                      <li className="list-group-item">
                        Sườn nướng: <b>4</b>
                      </li>
                      <li className="list-group-item">
                        Salad: <b>300g</b>
                      </li>
                      <li className="list-group-item">
                        Nước ngọt: <b>3</b>
                      </li>
                      <li className="list-group-item">
                        <h1>
                          <b>200.000</b>
                        </h1>
                        <p>VNĐ</p>
                      </li>
                    </ul>
                  </div>
                  <div className="card-footer text-center">
                    <button
                      className="btn btn-block btn-pizza"
                      id="btn-pizza-medium"
                    >
                      Chọn
                    </button>
                  </div>
                </div>
              </div>
              <div className="col-sm-4">
                <div className="card">
                  <div className="card-header card-header-large text-center">
                    <h3>L (large)</h3>
                  </div>
                  <div className="card-body text-center">
                    <ul className="list-group list-group-flush">
                      <li className="list-group-item">
                        Đường kính: <b>35cm</b>
                      </li>
                      <li className="list-group-item">
                        Sườn nướng: <b>8</b>
                      </li>
                      <li className="list-group-item">
                        Salad: <b>500g</b>
                      </li>
                      <li className="list-group-item">
                        Nước ngọt: <b>4</b>
                      </li>
                      <li className="list-group-item">
                        <h1>
                          <b>250.000</b>
                        </h1>
                        <p>VNĐ</p>
                      </li>
                    </ul>
                  </div>
                  <div className="card-footer text-center">
                    <button
                      className="btn btn-block btn-pizza"
                      id="btn-pizza-large"
                    >
                      Chọn
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Size