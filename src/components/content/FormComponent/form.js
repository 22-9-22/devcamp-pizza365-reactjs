import { Component } from "react";

class Form extends Component {
    render() {
        return (
            <div>
                <div id="order" className="row">
                    <div className="col-sm-12 text-center p-4 mt-4 text-warning">
                        <h2>
                            <b className="p-2 border-bottom">Gửi đơn hàng</b>
                        </h2>
                    </div>
                    <div className="col-sm-12 p-2 jumbotron">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="form-group">
                                    <label>Họ và tên</label>
                                    <input type="text" className="form-control" id="inp-fullname" placeholder="Họ và tên"/>
                                </div>
                                <div className="form-group">
                                    <label>Email</label>
                                    <input type="email" className="form-control" id="inp-email" placeholder="vd: a@gmail.com"/>
                                    <p id="result"></p>
                                </div>
                                <div className="form-group">
                                    <label>Điện thoại</label>
                                    <input type="tel" className="form-control" id="inp-dien-thoai" placeholder="Từ 10 đến 12 số"/>
                                    <p id="result-phone"></p>
                                </div>
                                <div className="form-group">
                                    <label>Địa chỉ</label>
                                    <input type="text" className="form-control" id="inp-dia-chi" placeholder="Địa chỉ"/>
                                </div>
                                <div className="form-group">
                                    <label>Mã giảm giá</label>
                                    <input type="text" className="form-control" id="inp-discount" placeholder="Mã giảm giá"/>
                                </div>
                                <div className="form-group">
                                    <label>Lời nhắn</label>
                                    <input type="text" className="form-control" id="inp-message" placeholder="Lời nhắn"/>
                                </div>
                                <button type="button" className="btn w-100" id="btn-order">Gửi</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Form