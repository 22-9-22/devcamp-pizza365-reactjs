import { Component } from "react";
import { Type } from "react-bootstrap-icons";
import ImageBacon from "../../../assets/images/bacon.jpg";
import ImageHawaii from "../../../assets/images/hawaiian.jpg";
import ImageSeafood from "../../../assets/images/seafood.jpg";
import Drink from "../DrinkComponent/drink";
import Form from "../FormComponent/form";
import Introduce from "../IntroduceComponent/introduce";
import Size from "../SizeComponent/size";

class Content extends Component {
  render() {
    return (
      <div>
        <div className="container content">
          <div className="row">
            <div className="col-sm-12">
                <Introduce/>
                <Size/>
                <Type/>
                <Drink/>
                <Form/>
              <div>
                <div id="modalConfirmOrder" className="modal" role="dialog">
                  <div className="modal-dialog" role="document">
                    <div className="modal-content">
                      <div className="modal-header">
                        <h5 className="modal-title">Thông tin đơn hàng</h5>
                        <button
                          type="button"
                          className="close"
                          data-dismiss="modal"
                          aria-label="Close"
                        >
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div className="modal-body">
                        <div className="row">
                          <div className="col-sm-12">
                            <div className="form-group">
                              <label>Họ và tên</label>
                              <input
                                type="text"
                                className="form-control"
                                id="modal-inp-fullname"
                                placeholder="Họ và tên"
                              />
                            </div>
                            <div className="form-group">
                              <label>Email</label>
                              <input
                                type="text"
                                className="form-control"
                                id="modal-inp-email"
                                placeholder="Email"
                              />
                            </div>
                            <div className="form-group">
                              <label>Điện thoại</label>
                              <input
                                type="text"
                                className="form-control"
                                id="modal-inp-dien-thoai"
                                placeholder="Điện thoại"
                              />
                            </div>
                            <div className="form-group">
                              <label>Địa chỉ</label>
                              <input
                                type="text"
                                className="form-control"
                                id="modal-inp-dia-chi"
                                placeholder="Địa chỉ"
                              />
                            </div>
                            <div className="form-group">
                              <label>Mã giảm giá</label>
                              <input
                                type="text"
                                className="form-control"
                                id="modal-inp-discount"
                                placeholder="Mã giảm giá"
                              />
                            </div>
                            <div className="form-group">
                              <label>Lời nhắn</label>
                              <input
                                type="text"
                                className="form-control"
                                id="modal-inp-message"
                                placeholder="Lời nhắn"
                              />
                            </div>
                            <div className="form-group">
                              <label>Thông tin chi tiết </label>
                              <textarea
                                name=""
                                id="modalTextPizzaInfo"
                                cols="30"
                                rows="5"
                                className="form-control"
                              ></textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="modal-footer">
                        <button
                          type="button"
                          className="btn btn-danger"
                          data-dismiss="modal"
                          id="btn-back"
                        >
                          Quay lại (Sửa)
                        </button>
                        <button
                          type="button"
                          className="btn btn-warning"
                          id="btn-confirm-order"
                        >
                          Tạo đơn
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="modal" role="dialog" id="modalThanks">
                  <div className="modal-dialog" role="document">
                    <div className="modal-content" id="modalThankContent">
                      <div className="modal-header">
                        <h5 className="modal-title">
                          Cám ơn bạn đã đặt hàng tại Pizza 365. Mã đơn hàng của
                          bạn là:
                        </h5>
                        <button
                          type="button"
                          className="close"
                          data-dismiss="modal"
                          aria-label="Close"
                        >
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div className="modal-body">
                        <div className="row">
                          <div className="col-sm-3">
                            <p>Mã đơn hàng</p>
                          </div>
                          <div className="col-sm-9">
                            <input
                              type="text"
                              className="form-control"
                              id="inpOrderId"
                            />
                          </div>
                        </div>
                      </div>
                      <div className="modal-footer">
                        <button
                          type="button"
                          className="btn btn-secondary"
                          data-dismiss="modal"
                        >
                          Đóng
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Content;
