import { Component } from "react";

class HeaderComponent extends Component {
  render() {
    return (
      <div>
        <div className="row">
          <div className="col-sm-12">
            <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-light">
              <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon"></span>
              </button>
              <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav nav-fill w-100">
                  <li className="nav-item active">
                    <a className="nav-link" href="#">
                      Trang chủ
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#size">
                      Chọn size
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#type">
                      Chọn loại Pizza
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#order">
                      Thông tin đơn hàng
                    </a>
                  </li>
                </ul>
              </div>
            </nav>
          </div>
        </div>
      </div>
    );
  }
}

export default HeaderComponent;
