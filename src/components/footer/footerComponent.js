import { Component } from "react";
import { Facebook, Instagram, Snapchat, Twitter, Linkedin, ArrowUp, Pinterest } from "react-bootstrap-icons";

class FooterComponent extends Component {
  render() {
    return (
      <div>
        <div className="container-fluid footer p-5">
          <div className="row text-center">
            <div className="col-sm-12">
              <h4 className="m-2">FOOTER</h4>
              <a href="#" className="btn btn-dark m-3">
                To the top&nbsp;
                <ArrowUp />
              </a>
              <div className="m-2">
                <Facebook />
                &nbsp;
                <Instagram />
                &nbsp;
                <Snapchat />
                &nbsp;
                <Pinterest />
                &nbsp;
                <Twitter />
                &nbsp;
                <Linkedin />
              </div>
              <h6 className="m-2">Powered by TUTT1</h6>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default FooterComponent